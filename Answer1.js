function cekAnagram(a, b) {
  let array = {};
  if (a === b) {
    return true;
  }
  if (a.length !== b.length) {
    return false;
  }
  for (let i = 0; i < a.length; i++) {
    array[a[i]] = (array[a[i]] || 0) + 1;
  }
  console.log(array);
  for (let j = 0; j < b.length; j++) {
    if (!array[b[j]]) {
      return false;
    }
    array[b[j]]--;
  }
  return true;
}

console.log(cekAnagram("anagram", "nagaram"));
